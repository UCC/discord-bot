use aes::Aes128;
use base64;
use block_modes::block_padding::Pkcs7;
use block_modes::{BlockMode, Cbc};
use chrono::{prelude::Utc, DateTime};
use rand::Rng;
use serenity::model::user::User;
use std::str;

type Aes128Cbc = Cbc<Aes128, Pkcs7>;
pub static TOKEN_LIFETIME: i64 = 300; // 5 minutes

lazy_static! {
    static ref KEY: [u8; 32] = rand::thread_rng().gen::<[u8; 32]>();
    static ref IV: [u8; 16] = [0; 16];
    static ref CIPHER: Aes128Cbc = Aes128Cbc::new_var(KEY.as_ref(), IV.as_ref()).unwrap();
}

fn text_encrypt(plaintext: &str) -> String {
    base64::encode(CIPHER.clone().encrypt_vec(plaintext.as_bytes()))
}

fn text_decrypt(ciphertext: &str) -> Option<String> {
    guard!(let Ok(cipher_vec) = base64::decode(ciphertext) else {
        warn!("Unable to decode base64 text");
        return None
    });
    guard!(let Ok(decrypted_vec) = CIPHER.clone().decrypt_vec(&cipher_vec) else {
        warn!("Text decryption failed");
        return None
    });
    guard!(let Ok(decrypted_token) = str::from_utf8(decrypted_vec.as_slice()) else {
        warn!("Invalid utf8 in text");
        return None
    });
    Some(decrypted_token.to_owned())
}

pub fn generate_token(discord_user: &User, username: &str) -> String {
    // if username doesn't exist : throw error
    let timestamp = Utc::now().to_rfc3339();
    let payload = format!(
        "{},{},{}",
        timestamp,
        discord_user.id.0.to_string(),
        username
    );
    info!("Token generated for {}: {}", discord_user.name, &payload);
    text_encrypt(&payload)
}

#[derive(Debug)]
pub enum TokenError {
    DiscordIdMismatch,
    TokenExpired,
    TokenInvalid,
}
impl std::fmt::Display for TokenError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub fn parse_token(discord_user: &User, encrypted_token: &str) -> Result<String, TokenError> {
    guard!(let Some(token) = text_decrypt(encrypted_token) else {
        return Err(TokenError::TokenInvalid)
    });
    let token_components: Vec<_> = token.splitn(3, ',').collect();
    info!(
        "Verification attempt from '{}'(uid: {}) for account '{}' with token from {}",
        discord_user.name, token_components[1], token_components[2], token_components[0]
    );
    let token_timestamp =
        DateTime::parse_from_rfc3339(token_components[0]).expect("Invalid date format");
    let token_discord_user = token_components[1];
    let token_username = token_components[2];
    if token_discord_user != discord_user.id.0.to_string() {
        warn!("... attempt failed : DiscordID mismatch");
        return Err(TokenError::DiscordIdMismatch);
    }
    let time_delta_seconds = Utc::now().timestamp() - token_timestamp.timestamp();
    if time_delta_seconds > TOKEN_LIFETIME {
        warn!(
            "... attempt failed : token expired ({} seconds old)",
            time_delta_seconds
        );
        return Err(TokenError::TokenExpired);
    }
    info!(
        "... verification successful (token {} seconds old)",
        time_delta_seconds
    );
    Ok(token_username.to_owned())
}
