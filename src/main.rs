#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;
extern crate indexmap;
extern crate simplelog;
#[macro_use]
extern crate guard;

#[macro_use]
extern crate diesel;
extern crate ldap3;

extern crate reqwest;

extern crate tokio;

use simplelog::*;
use std::fs::File;

use serenity::client::Client;

#[macro_use]
mod util;
mod config;
mod database;
mod ldap;
mod reaction_roles;
mod serenity_handler;
mod token_management;
mod user_management;
mod voting;

use config::SECRETS;
use serenity_handler::Handler;

#[tokio::main]
async fn main() {
    CombinedLogger::init(vec![
        TermLogger::new(LevelFilter::Info, Config::default(), TerminalMode::Mixed),
        WriteLogger::new(
            LevelFilter::Info,
            Config::default(),
            File::create("ucc-bot.log").unwrap(),
        ),
    ])
    .unwrap();

    // ical::fetch_latest_ical().wait();

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::builder(&SECRETS.discord_token)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform
    // exponential backoff until it reconnects.
    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
